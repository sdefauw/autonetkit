% for i in node.interfaces:
%if not i.loopback:
/sbin/ifconfig ${i.id} up
%endif
% endfor

route del default
/sbin/ifconfig lo 127.0.0.1 up
/etc/init.d/ssh start
/etc/init.d/hostname.sh
% if node.ssh.use_key:
chown -R root:root /root
chmod 755 /root
chmod 755 /root/.ssh
chmod 644 /root/.ssh/authorized_keys
% endif

brctl addbr br0
% for i in node.interfaces:
%if not i.loopback:
brctl addif br0 ${i.id}
%endif
% endfor
brctl stp br0 on
ifconfig br0 up