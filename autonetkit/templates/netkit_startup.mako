% for i in node.interfaces:
	% if i.dhclient:
/sbin/ifconfig ${i.id} 0.0.0.0 promisc up
dhclient ${i.id}
	% else :
		%if i.loopback:
/sbin/ifconfig ${i.id} ${i.ipv4_address[0]} netmask ${i.ipv4_subnet.netmask} broadcast ${i.ipv4_subnet.broadcast} up
		%else:
/sbin/ifconfig ${i.id} ${i.ipv4_address} netmask ${i.ipv4_subnet.netmask} broadcast ${i.ipv4_subnet.broadcast} up
		%endif
	% endif
% endfor

route del default
/sbin/ifconfig lo 127.0.0.1 up
% for i in node.interfaces:
	%if i.ipv4_gateway:
		% for ip in i.ipv4_gateway:
route add default gw ${ip} ${i.id}
		% endfor
    %endif
% endfor

/etc/init.d/ssh start
/etc/init.d/hostname.sh

% if node.ssh.use_key:
chown -R root:root /root
chmod 755 /root
chmod 755 /root/.ssh
chmod 644 /root/.ssh/authorized_keys
% endif

%if node.dhcpd:
/etc/init.d/dhcp3-server start
%if node.dhcpd.failover:
#If problem to startup
sleep 90
/etc/init.d/dhcp3-server restart
%endif
%endif
%if node.dhcrelay:
dhcrelay ${node.dhcrelay.ipserver}
%endif

% if node.zebra:
/etc/init.d/zebra start
%endif