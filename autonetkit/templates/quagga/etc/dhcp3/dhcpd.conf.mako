%if node.dhcpd:
%for f in node.dhcpd.failover:
failover peer "${f.name}" {
	${f.type};
    address ${f.address};
    port ${f.port};
    peer address ${f.peer_add};
    peer port ${f.port};
    max-response-delay ${f.max_response_delay};
    max-unacked-updates ${f.max_unacked_updates};
    load balance max seconds ${f.load_bal};
	%if f.mclt:
    mclt ${f.mclt};
    %endif
	%if f.split:
    split ${f.split};
	%endif
}

%endfor

% for s in node.dhcpd.subnet:
subnet ${s.subnet} netmask ${s.netmask} {
	option subnet-mask ${s.netmask};
	option broadcast-address ${s.broadcast};
	%if s.routers:
    option routers ${s.routers};
    %endif
	%if s.dns:
    option domain-name-servers ${s.dns};
	%endif
	%if s.lease_time:
    default-lease-time ${s.lease_time};
    %endif
	%if s.lease_time_max:
    max-lease-time ${s.lease_time_max};
    %endif
	%if s.failover:
    pool {
	    failover peer "${s.failover}";
        range ${s.range};
    }
	%else:
    range ${s.range};
	%endif
}

% endfor
%endif