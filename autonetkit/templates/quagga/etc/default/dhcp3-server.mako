%if node.dhcpd:
# Listening interfaces
%if node.dhcpd.interfaces:
INTERFACES="${node.dhcpd.interfaces}"
%endif
%endif