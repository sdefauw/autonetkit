%if node.dhcrelay:
######## DHCP RELAY ########
# Address of DHCP server
SERVERS="${node.dhcrelay.ipserver}"

# Listening interfaces
%if node.dhcpd.interfaces:
INTERFACES="${node.dhcpd.interfaces}"
%endif

#Additional Options (by silent running)
OPTIONS="-q"
%endif