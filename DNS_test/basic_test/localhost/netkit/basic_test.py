from Exscript.util.start import start
from Exscript.util.file  import get_hosts_from_file
from Exscript import Account
from Exscript.util.match import first_match
import string

error = []
good = []


def do_something(job, host, conn):
    conn.execute("hostname -v")
    name_tmp = repr(conn.response)
    begin = name_tmp.rindex('\\n')
    end = len(name_tmp)-5
    
    name = name_tmp[begin+2:end]
    conn.execute("dig @6.6.6.6 someWebsite")
    response = repr(conn.response)
    if("Got answer:" in response):
        good.append('\tDevice {}... ok'.format(name))
    else:
        error.append('\tDevice {}... ko'.format(name))
       
    conn.send('exit\r')
    conn.close()

def show():
    for i in good:
        print i
    for i in error:
        print i
    print '\tSucceed = {} ; Error = {}'.format(len(good), len(error))
 
hosts = get_hosts_from_file('hosts.txt')

accounts = [Account('root', '1234')]
start(accounts, hosts, do_something)

print '\nTest basic DNS configuration'
show()
        
