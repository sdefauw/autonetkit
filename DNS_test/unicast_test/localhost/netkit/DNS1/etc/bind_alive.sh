#!/bin/bash
while sleep 10;
do
	DNSUP=`dig @localhost localhost. A +short`
	if [[ "$DNSUP" =~ "connection timed out" ]]
	then
			if [[ `ifconfig` =~ .*6.6.6.6.* ]]
			then
				echo "Shutting down lo:2"
				/sbin/ifconfig lo:2 down
			fi
			if [[ `ifconfig` =~ .*6.6.3.3.* ]]
			then
				echo "Shutting down lo:3"
				/sbin/ifconfig lo:3 down
			fi
	else 
			if ! [[ `ifconfig` =~ .*6.6.6.6.* ]]
			then
				echo "Starting up lo:2"
				/sbin/ifconfig lo:2 6.6.6.6 netmask 255.255.255.255 broadcast 192.168.1.3 up
			fi
			if ! [[ `ifconfig` =~ .*6.6.3.3.* ]]
			then
				echo "Starting up lo:3"
				/sbin/ifconfig lo:3 6.6.3.3 netmask 255.255.255.255 broadcast 192.168.1.3 up
			fi
	fi
done
