from Exscript.util.start import start
from Exscript.util.file  import get_hosts_from_file
from Exscript import Account
from Exscript.util.match import first_match
import string

error = []
success = []
R2 = ['router2','host3']
R1 = ['router1','host2','host1']


def do_something(job, host, conn):
    conn.execute("hostname -v")
    name_tmp = repr(conn.response)
    begin = name_tmp.rindex('\\n')
    end = len(name_tmp)-5
    
    name = name_tmp[begin+2:end]
    conn.execute("traceroute 6.6.6.6")
    response = repr(conn.response)
    if(name in R1):
        if('10.0.0.10' in response or '10.0.0.13' in response or '6.6.6.6' in response):
            success.append('\tDevice {}... ok'.format(name))
        else:
            error.append('\tDevice {}... ko'.format(name))
    elif(name in R2):
        if('10.0.0.5' in response or '6.6.6.6' in response):
            success.append('\tDevice {}... ok'.format(name))
        else:
            error.append('\tDevice {}... ko'.format(name))
       
    conn.send('exit\r')
    conn.close()

def show():
    for i in success:
        print i
    for i in error:
        print i
    print '\tSucceed = {} ; Error = {}'.format(len(success), len(error))

   
hosts = get_hosts_from_file('hosts_unicast.txt')
#hosts = 'ssh://172.16.0.5' #R1

accounts = [Account('root', '1234')]
start(accounts, hosts, do_something)

print '\nTest DNS unicast configuration'
show()
        
