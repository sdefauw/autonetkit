import time
from Exscript.util.start import start
from Exscript.util.file  import get_hosts_from_file
from Exscript import Account
from Exscript.util.match import first_match
import string

error = []
success = []
R2 = ['router2','host3']
R1 = ['router1','host2','host1']
nb_router_2_DNS = {}
DNS = []

def check_route(job, host, conn):
    conn.execute("hostname -v")
    name_tmp = repr(conn.response)
    begin = name_tmp.rindex('\\n')
    end = len(name_tmp)-5
    
    name = name_tmp[begin+2:end]
    if('DNS' not in name) :     
        conn.execute("traceroute 6.6.6.6")
        response = repr(conn.response)
        nb_router_2_DNS[name] = response.count('ms')/3
    else:
        DNS.append(name)

def kill_dns_server(job, host, conn):
    conn.execute("hostname -v")
    name_tmp = repr(conn.response)
    begin = name_tmp.rindex('\\n')
    end = len(name_tmp)-5
    
    name = name_tmp[begin+2:end]
    
    if(name == 'DNS2'):
        conn.execute("halt")
        success.append('\t\tdone')
        print 'Waiting...'
        time.sleep(20)
        
def check_newRoute(job, host, conn):
    conn.execute("hostname -v")
    name_tmp = repr(conn.response)
    begin = name_tmp.rindex('\\n')
    end = len(name_tmp)-5
    
    name = name_tmp[begin+2:end]
    if(name in R2):
        conn.execute("traceroute 6.6.6.6")
        response = repr(conn.response)
        new_nb_router = response.count('ms')/3
        success.append('\t\tI\'ve found {} routers from {} to the DNS server instead of {}'.format(new_nb_router, name, nb_router_2_DNS[name]))

def check_dig(job, host, conn): 
    conn.execute("hostname -v")
    name_tmp = repr(conn.response)
    begin = name_tmp.rindex('\\n')
    end = len(name_tmp)-5
    
    name = name_tmp[begin+2:end]
    if('DNS' not in name):    
        conn.execute("dig @6.6.6.6 someWebsite")
        response = repr(conn.response)
        if("Got answer:" in response):
            success.append('\t\tDevice {}... ok'.format(name))
        else:
            error.append('\t\tDevice {}... ko'.format(name))
       
    conn.send('exit\r')
    conn.close()
    
def show():
    print '\tFound {} DNS server'.format(len(DNS))
    for i in success:
        print i
    for i in error:
        print i
    print '\tSucceed = {} ; Error = {}'.format(len(success), len(error))

   
hosts = get_hosts_from_file('host_w_kill.txt')
new_hosts = get_hosts_from_file('hosts_unicast.txt')
#hosts = 'ssh://172.16.0.5' #R1


accounts = [Account('root', '1234')]
start(accounts, hosts, check_route)
success.append('\tKilling 1 DNS server...')
start(accounts, hosts, kill_dns_server)
success.append('\tI will now check if routes have changed')
start(accounts, new_hosts, check_newRoute)
success.append('\tI will now check if all hosts can reach the dns server')
start(accounts, new_hosts, check_dig)

print '\nTest DNS unicast configuration with death of a DNS server'
show()
        
